**Pack to certificate Magento 2 - Associate-Developer**
This pack will help you to certificate in Magento 2

## How do you use this pack?
You need the packege *certificationy/certificationy-cli*. You will find in github, below link:
GitHub: *http://github.com/certificationy/certificationy-cli*

1. You need to clone package. *git clone https://github.com/certificationy/certificationy-cli.git*
2. In folder /certificationy-cli/, in composer.jon, add the code:
```json
"repositories": [{
    "type": "vcs",
    "url": "git@bitbucket.org:TiagoTescaRoramos/magento2-pack.git"
}]
```
3. In "require", add the code:
```json
"tiagotescaro/magento2-pack": "dev-master"
```
4. You will need composer in your server. You can install composer in your server with code below:
```shell
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e5325b19b381bfd88ce90a5ddb7823406b2a38cff6bb704b0acc289a09c8128d4a8ce2bbafcd1fcbdc38666422fe2806') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```
With composer in your server, execute command: ``` composer update tiagotescaro/magento2-pack ```

5. In folder /certificationy-cli/, in file config.yml, add in paths:
``` vendor/tiagotescaro/magento2-pack/data ```. 
For exemplo:
```yml
paths: ["vendor/certificationy/symfony-pack/data", "vendor/certificationy/php-pack/data", "vendor/tiagotescaro/magento2-pack/data"]
```
6. In folder /certificationy-cli/, execute ```php certificationy.php --list```. You will see all the tests.
7. In folder /certificationy-cli/, you execute tests Magento 2, for exemplo, execute o command: ```php certificationy.php start "Magento-2-Certified-Associate-Developer"```
